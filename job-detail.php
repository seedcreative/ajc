<?php include_once('header.php'); ?>
<main class="main">
	<div class="container">
		<div class="row">
			<div class="content col-md-9">
				<div class="card jobdetail">
					<h1 class="card__title">Marketing Officer</h1>
					<div class="jobdetail__meta">
						<div class="row">
							<div class="col-md-3 jobdetail__meta__list jobdetail__meta--salary">
								<i class="aj-salary"></i>
								<span data-toggle="tooltip" data-placement="top" title="Salary">Negotiable</span>	
							</div>

							<div class="col-md-3 jobdetail__meta__list jobdetail__meta--type">
								<i class="aj-jobtype"></i>
								<span data-toggle="tooltip" data-placement="top" title="Job Type">Full Time</span>		
							</div>

							<div class="col-md-3 jobdetail__meta__list jobdetail__meta--deadline">
								<i class="aj-deadline"></i>
								<span data-toggle="tooltip" data-placement="top" title="Deadline">March 8th, 2018</span>
							</div>

							<div class="col-md-3 jobdetail__meta__list jobdetail__meta--vacancy">
								<i class="aj-noofvacancies"></i>
								<span data-toggle="tooltip" data-placement="top" title="Number of Vacancy">Vacancy for 1</span>
							</div>

							<div class="col-md-3 jobdetail__meta__list jobdetail__meta--experience">
								<i class="aj-experience"></i>
								<span data-toggle="tooltip" data-placement="top" title="Experience Required">2 Year(s)</span>
							</div>

							<div class="col-md-3 jobdetail__meta__list jobdetail__meta--category">
								<i class="aj-category"></i>
								<span data-toggle="tooltip" data-placement="top" title="Job Category">Co-Operative</span>
							</div>

							<div class="col-md-3 jobdetail__meta__list jobdetail__meta--level">
								<i class="aj-joblevel"></i>
								<span data-toggle="tooltip" data-placement="top" title="Level">Entry Level</span>
							</div>
						</div>
					</div>
					<div class="jobdetail__edu">
						<h3>Education Detail</h3>
						<dl class="jobdetail__edu__list d-flex flex-wrap">
							<dt>Education Level</dt>
							<dd>: Bachelors/Diploma</dd>
							<dt>Faculty</dt>
							<dd>: BBA, BBS</dd>
						</dl>
					</div>
					<div class="jobdetail__desc">
						<h3>Job Description</h3>
						<ul class="list--dotted">
							<li>Develop and implement marketing strategies</li>
							<li>Achieve agreed upon marketing plan which will meet both personal and business goals of expanding the customer base</li>
							<li>Work within the sales and support teams for the achievement of customer satisfaction, revenue generation, and long-term account goals in line with company vision and values</li>
							<li>Dealing with inquiries and complaints</li>
							<li>Help organize business promotion</li>
						</ul>
					</div>
					<div class="jobdetail__nicetohave">
						<h3>Nice to have</h3>
						<ul class="list--dotted">
							<li>Bachelors Degree in Business, Marketing or Related Fields from an accredited Institution (Major Specialization in Marketing)</li>
							<li>Minimum 1 Yrs of experience in Sales/marketing</li>
							<li>Basic Computer Skills</li>
							<li>Decision-making ability and a sense of responsibility</li>
							<li>Basic understanding of market dynamics and requirements</li>
						</ul>
					</div>
					<div class="jobdetail__howtoapply">
						<h3>How to apply</h3>
						<p>Email us your detailed CV with Cover letter to <strong>marketing.wintech@gmail.com</strong></p>
						<p>OR,</p>
						<div class="jobdetail__howtoapply--btngroup" >
							<a href="#" class="btn btn-md btn-primary">
								<i class="aj-applyjob"></i>
								Apply for this job
							</a>
							<a href="#" class="btn btn-md btn-secondary">
								<i class="aj-sent-mail"></i>
								Send Direct Email
							</a>
							<a href="#" class="btn btn-md btn-danger">
								<i class="aj-savejob"></i>
								Save Job
							</a>
						</div>
					</div>
				</div>
				<div class="card sharethis">
					<div class="sharethis__inline d-flex align-items-center">
						<h4>Share this job</h4>
						<div class="sharethis-inline-share-buttons"></div>
					</div>
				</div>
			</div>
			<aside class="sidebar col-md-3">
				<div class="card widget ">
					<div class="widget__company d-flex flex-column align-items-center">
						<div class="widget__company--image">
							<picture>
								 <source media="(min-width: 768px)" srcset="images/wintec.jpg">
								<img src="images/wintec_md.jpg" alt="Wintec" class="img-fluid">
							</picture>
						</div>
						<h2>Wintec Solutions</h2>
						<ul class="widget__company--det">
							<li>
								<i class="aj-location"></i>
								New Baneshwor, Kathmandu
							</li>
							<li>
								<i class="aj-website"></i>
								www.wintec.com.np
							</li>
						</ul>
						<div class="widget__company--btngroup">
							<a href="#" class="btn btn-md btn-primary">
								<i class="aj-applyjob"></i>
								Apply for this job
							</a>
							<a href="#" class="btn btn-md btn-secondary">
								<i class="aj-sent-mail"></i>
								Send Direct Email
							</a>
							<a href="#" class="btn btn-md btn-danger">
								<i class="aj-savejob"></i>
								Save Job
							</a>
						</div>
					</div>
				</div>
				<div class="card sharethis">
					<div class="d-flex flex-column">
						<h4>Share this job</h4>
						<div class="sharethis-inline-share-buttons"></div>
					</div>
				</div>
			</aside>
		</div>
	</div>
</main>
<?php include_once('footer.php'); ?>