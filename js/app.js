//jQuery is required to run this code
$(document).ready(function() {
    scaleVideoContainer();
    initBannerVideoSize('.video__container .poster img');
    initBannerVideoSize('.video__container .filter');
    initBannerVideoSize('.video__container video');
    $(window).on('resize', function() {
        scaleVideoContainer();
        scaleBannerVideoSize('.video__container .poster img');
        scaleBannerVideoSize('.video__container .filter');
        scaleBannerVideoSize('.video__container video');
    });

    //Owl Carousel
    //Top Employers
    $('#topemployers').owlCarousel({
        loop:true,
        margin:20,
        autoplay: true,
        autoplayTimeout: 2000,
        autoplayHoverPause: true,
        nav:false,
        dots: false,
        responsive:{
            0:{
                items: 1
            },
            600:{
                items: 2
            }
        }
    });

    //Admin Table Data Table
    var job_table = $('#jobstable').DataTable({
        "dom": '<"card filter d-flex flex-row align-items-center justify-content-between"i f>rt<"pager card d-flex flex-row justify-content-between align-items-center"i p>',
        "order": [[ 0, "asc" ]],
        "columnDefs": [
        {
            "targets": [ 0 ],
            "visible": false,
            "searchable": false
        }],
        "info": true,
        "pagingType": "full_numbers",
        "lengthMenu": [8],
        "language": {
            "searchPlaceholder": "What are you looking for...",
            "search": "_INPUT_",
        }
    });

    $("#jobstable_filter input").addClass("form-control form-dark");
    $("#jobstable_filter label").append("<i class='aj-search'></i>");

    //Initializing Tool Tips
    
});

    $(function () {
      $('[data-toggle="tooltip"]').tooltip()
    });

function scaleVideoContainer() {

    var height = $('.search').height() + 80;
    var unitHeight = parseInt(height) + 'px';
    $('.video').css('height',unitHeight);

}

function initBannerVideoSize(element){

    $(element).each(function(){
        $(this).data('height', $(this).height());
        $(this).data('width', $(this).width());
    });

    scaleBannerVideoSize(element);

}

function scaleBannerVideoSize(element){

    var windowWidth = $(window).width(),
    windowHeight = $(window).height() + 5,
    videoWidth,
    videoHeight;

    // console.log(windowHeight);

    $(element).each(function(){
        var videoAspectRatio = $(this).data('height')/$(this).data('width');

        $(this).width(windowWidth);

        if(windowWidth < 1000){
            videoHeight = windowHeight;
            videoWidth = videoHeight / videoAspectRatio;
            $(this).css({'margin-top' : 0, 'margin-left' : -(videoWidth - windowWidth) / 2 + 'px'});

            $(this).width(videoWidth).height(videoHeight);
        }

        $('.video .video__container video').addClass('fadeIn animated');

    });
}