<div class="jobs__latest">
	<h2>
		<div class="icon jobs__latest--circle">
			<i class="aj-latestjobs"></i>
		</div>Latest Jobs
	</h2>
	<table class="jobstable table table-responsive-md">
		<tr class="jobstable__item">
			<td class="jobstable__image">
				<figure>
					<img src="images/company.png" alt="New Hope Agro Business">
				</figure>
			</td>
			<td class="jobstable__det">
				<h5>
					<a href="#">Lower Secondary English Teacher</a>
				</h5>
				<a href="" class="jobstable__emp">
					Nalanda Vidhya Mandir
				</a>
				<span class="jobstable__deadline">
					<i class="aj-deadline"></i> 234 Days Left
				</span>
			</td>
			<td class="jobstable__emploc">
				<i class="aj-location"></i> Sano Thimi, Bhaktapur
			</td>
			<td>
				<span class="btn btn-sm btn-secondary">Full Time</span>
			</td>
		</tr>
		<tr class="jobstable__item">
			<td class="jobstable__image">
				<figure>
					<img src="images/company.png" alt="New Hope Agro Business">
				</figure>
			</td>
			<td class="jobstable__det">
				<h5>
					<a href="#">Lower Secondary English Teacher</a>
				</h5>
				<a href="" class="jobstable__emp">
					Nalanda Vidhya Mandir
				</a>
				<span class="jobstable__deadline">
					<i class="aj-deadline"></i> 234 Days Left
				</span>
			</td>
			<td class="jobstable__emploc">
				<i class="aj-location"></i> Sano Thimi, Bhaktapur
			</td>
			<td>
				<span class="btn btn-sm btn-secondary">Full Time</span>
			</td>
		</tr>
		<tr class="jobstable__item">
			<td class="jobstable__image">
				<figure>
					<img src="images/company.png" alt="New Hope Agro Business">
				</figure>
			</td>
			<td class="jobstable__det">
				<h5>
					<a href="#">Lower Secondary English Teacher</a>
				</h5>
				<a href="" class="jobstable__emp">
					Nalanda Vidhya Mandir
				</a>
				<span class="jobstable__deadline">
					<i class="aj-deadline"></i> 234 Days Left
				</span>
			</td>
			<td class="jobstable__emploc">
				<i class="aj-location"></i> Sano Thimi, Bhaktapur
			</td>
			<td>
				<span class="btn btn-sm btn-secondary">Full Time</span>
			</td>
		</tr>
		<tr class="jobstable__item">
			<td class="jobstable__image">
				<figure>
					<img src="images/company.png" alt="New Hope Agro Business">
				</figure>
			</td>
			<td class="jobstable__det">
				<h5>
					<a href="#">Lower Secondary English Teacher</a>
				</h5>
				<a href="" class="jobstable__emp">
					Nalanda Vidhya Mandir
				</a>
				<span class="jobstable__deadline">
					<i class="aj-deadline"></i> 234 Days Left
				</span>
			</td>
			<td class="jobstable__emploc">
				<i class="aj-location"></i> Sano Thimi, Bhaktapur
			</td>
			<td>
				<span class="btn btn-sm btn-secondary">Full Time</span>
			</td>
		</tr>
	</table>
	<div class="jobs__showmore">
		<a href="#" class="btn btn-primary btn-sm">Load more jobs</a>
	</div>
</div>