<footer class="footer">
	<div class="footer__top">
		<div class="container">
			<div class="row">
				<div class="col-lg-3 footer__about">
					<a href="#" class="footer__logo"><img src="images/logo_white.png" alt="Asia Jobs Center"></a>
					<p>Nothing can be more constant than change; hence we do our bit to stimulate it.</p>
					<a href="#" class="text-link">read more...</a>
				</div>
				<div class="col-lg-3 footer__candidate">
					<h6>For Candidates</h6>
					<ul class="footer__links">
						<li>
							<a href="#">Register as a Candidate</a>
						</li>
						<li>
							<a href="#">Search Jobs</a>
						</li>
						<li>
							<a href="#">Post Resume</a>
						</li>
						<li>
							<a href="#">Career Services</a>
						</li>
					</ul>
				</div>
				<div class="col-lg-3 footer__employer">
					<h6>For Employer</h6>
					<ul class="footer__links">
						<li>
							<a href="#">Register as a Company</a>
						</li>
						<li>
							<a href="#">Search Resume</a>
						</li>
						<li>
							<a href="#">Post a Job</a>
						</li>
						<li>
							<a href="#">Recruitment Services</a>
						</li>
					</ul>
				</div>
				<div class="col-lg-3 footer__contact">
					<h6>Contact Us</h6>
					<ul class="footer__contact--det d-flex flex-column">
						<li class="d-flex">
							<i class="aj-location"></i>
							<span>Subidhanagar, Tinkune, Kathmandu, Nepal</span>
						</li>
						<li class="d-flex">
							<i class="aj-email"></i>
							<span>info@asiajobcenter.com</span>
						</li>
						<li class="d-flex">
							<i class="aj-phone"></i>
							<span>+977-01-2171469, 9849511834</span>
						</li>
					</ul>
				</div>
			</div>
		</div>
	</div>
	<div class="footer__bottom">
		<div class="container">
			<div class="row">
				<div class="col-md-6 col-lg-6 align-self-center">
					<p class="footer__copy ">Copyright &copy;2018. All Rights Reserved.</p>
				</div>
				<div class="col-md-6 col-lg-6">
					<ul class="footer__social d-flex justify-content-md-end">
						<li>
							<a href="#" class="facebook">
								<i class="aj-facebook"></i>
							</a>
						</li>
						<li>
							<a href="#" class="twitter">
								<i class="aj-twitter"></i>
							</a>
						</li>
						<li>
							<a href="#" class="linkedin">
								<i class="aj-linkedin"></i>
							</a>
						</li>
					</ul>
				</div>
			</div>
		</div>
	</div>
</footer>

<!-- Sign Up Modal -->
<div class="modal fade signup-modal" data-backdrop="static" tabindex="-1" role="dialog" aria-labelledby="JobSeekerSignUp" aria-hidden="true">
  <div class="modal-dialog modal-md">
    <div class="modal-content">
    	<div class="modal-header justify-content-center">
    		<h2>Register your account</h2>
			<span class="modal-close" data-dismiss="modal">
    			<i class="aj-cancel"></i>
    		</span>
    	</div>
    	<div class="modal-body">
    		<div class="tabs-heading d-flex justify-content-center">
    			<ul class="nav" id="tabs" role="tablist">
					<li class="nav-item">
						<a class="nav-link active" id="jobseeker-tab" data-toggle="tab" href="#jobseeker" role="tab" aria-controls="jobseeker" aria-selected="true">Job Seeker</a>
					</li>
					<li class="nav-item">
						<a class="nav-link" id="employer-tab" data-toggle="tab" href="#employer" role="tab" aria-controls="employer" aria-selected="true">Employer</a>
					</li>
				</ul>
    		</div>
			<div id="tabs-content" class="tab-content">
				<div class="tab-pane fade show active" id="jobseeker" role="tabpanel" aria-labelledby="jobseeker-tab">
					<form>
						<div class="form-group">
							<label class="form-label">Name</label>
							<input type="text" name="" placeholder="Please enter your name..." class="form-control form-dark form-round">
						</div>
						<div class="form-group">
							<label class="form-label">Email Address</label>
							<input type="email" name="" placeholder="Please enter your email address..." class="form-control form-dark form-round">
						</div>
						<div class="form-group">
							<label class="form-label">Password</label>
							<input type="password" name="" placeholder="Choose your password..." class="form-control form-dark form-round">
						</div>
						<div class="form-group">
							<label class="form-label">Re-Enter Password</label>
							<input type="password" name="" placeholder="Re-Enter your password..." class="form-control form-dark form-round">
						</div>
						<p class="terms">By clicking on the “<span>Register</span>” button, you agree to the Terms and Condition.</p>
						<button class="btn btn-primary btn-full text-uppercase">Register</button>
					</form>
				</div>
				<div class="tab-pane fade" id="employer" role="tabpanel" aria-labelledby="employer-tab">
					<form>
						<div class="form-group">
							<label class="form-label">Company Name</label>
							<input type="text" name="" placeholder="Please enter company name..." class="form-control form-dark form-round">
						</div>
						<div class="form-group">
							<label class="form-label">Email Address</label>
							<input type="email" name="" placeholder="Please enter your email address..." class="form-control form-dark form-round">
						</div>
						<div class="form-group">
							<label class="form-label">Password</label>
							<input type="password" name="" placeholder="Choose your password..." class="form-control form-dark form-round">
						</div>
						<div class="form-group">
							<label class="form-label">Re-Enter Password</label>
							<input type="password" name="" placeholder="Re-Enter your password..." class="form-control form-dark form-round">
						</div>
						<p class="terms">By clicking on the “<span>Register</span>” button, you agree to the Terms and Condition.</p>
						<button class="btn btn-primary btn-full text-uppercase">Register</button>
					</form>
				</div>
			</div>
    	</div>
    </div>
  </div>
</div>






<!-- Log In Modal -->
<div class="modal fade login-modal" data-backdrop="static" tabindex="-1" role="dialog" aria-labelledby="JobSeekerSignUp" aria-hidden="true">
  <div class="modal-dialog modal-md">
    <div class="modal-content">
    	<div class="modal-header justify-content-center">
    		<h2>Log In your account</h2>
    		<span class="modal-close" data-dismiss="modal">
    			<i class="aj-cancel"></i>
    		</span>
    	</div>
    	<div class="modal-body">
    		<form>
				<div class="form-group">
					<label class="form-label">Email Address</label>
					<input type="email" name="" placeholder="Please enter your email address..." class="form-control form-dark form-round">
				</div>
				<div class="form-group">
					<label class="form-label">Password</label>
					<input type="password" name="" placeholder="Choose your password..." class="form-control form-dark form-round">
				</div>
				<a href="#" class="forgot-password">Forgot your password?</a>
				<button class="btn btn-primary btn-full text-uppercase">Log In</button>
			</form>
    	</div>
    </div>
  </div>
</div>


<!-- JavaScripts -->
		<script src="https://cdnjs.cloudflare.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>
		<script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/js/bootstrap.bundle.min.js"></script>
		<script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/OwlCarousel2/2.3.3/owl.carousel.min.js"></script>
		<script src="js/jquery.autocomplete.js" type="text/javascript"></script>
		<script src="http://cdn.datatables.net/1.10.16/js/jquery.dataTables.min.js"></script>
		<script type='text/javascript' src='//platform-api.sharethis.com/js/sharethis.js#property=5ac07c30003b52001341b1b1&product=inline-share-buttons' async='async'></script>
		<script src="js/app.js" type="text/javascript"></script>
		<script type="text/javascript">
			$(function() {
				var jobs = [
				  'Android Developer', 'Admin Officer', 'Accountant', 'Assistant Manager', 'ASP .NET Developer', 'Barista', 'Business Representative', 'Business Research and Develpoment Manager', 'Country Manager', 'Software Development', 'Web Designer', 'UI/UX Designer', 'PHP Programer', 'Teacher', 'Lab Technician', 'Electrical Engineer'
				];

				var location = [
				  'Chabahil', 'Tinkune', 'Satdobaato', 'Koteshwor', 'New Baneshwor', 'Old Baneshwor', 'Maitidevi', 'Putalisadak', 'Kalanki', 'Kalimati', 'New Road', 'Dillibazar', 'Baghbazar', 'Maitighar', 'Kupondole', 'Pulchowk', 'Jawalakhel'
				];
			  	$("#job-field").autocomplete({
			    	source: [jobs]
			  	}); 

			  	$("#location-field").autocomplete({
			    	source: [location]
			  	}); 
			});
		</script>
	</body>
</html>