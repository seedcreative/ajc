<!DOCTYPE html>
<html>
	<head>
		<title>Asia Jobs Center</title>
		<meta charset="UTF-8">
		<meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=0"/>
		<!--FontAwesome CSS CDN-->
		<link rel="stylesheet" type="text/css" href="https://use.fontawesome.com/releases/v5.0.6/css/all.css">
		<!--Open Sans Google Fonts-->
		<link href="https://fonts.googleapis.com/css?family=Open+Sans:400,600,700" rel="stylesheet">
		<!--Quicksand Google Fonts-->
		<link href="https://fonts.googleapis.com/css?family=Quicksand:400,500,700" rel="stylesheet">

		<!-- Bootstrap 4 CDN-->
		<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/css/bootstrap.min.css">

		<!-- Main Site Style-->
		<link rel="stylesheet" type="text/css" href="https://cdnjs.cloudflare.com/ajax/libs/OwlCarousel2/2.3.3/assets/owl.carousel.css">
		<link rel="stylesheet" type="text/css" href="css/jquery.autocomplete.css">
		<link rel="stylesheet" type="text/css" href="css/site-style.css">
	</head>
	<body class="body">
		<header class="header">
			<div class="container">
				<nav class="navbar navbar-expand-lg">
				  <a class="header__logo" href="#"><img src="images/logo.png" alt="Asia Jobs Center"></a>
					
					<!-- Responsive Ham Menu-->
				  <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#main-nav" aria-controls="main-nav" aria-expanded="false" aria-label="Toggle navigation">
				    <span class="navbar-toggler-icon"></span>
				  </button>

				  <div class="collapse navbar-collapse" id="main-nav">
				    <ul class="nav ml-auto">
				      <li class="align-self-center nav__item">
				        <a href="#">Search Jobs</a>
				      </li>
				      <li class="nav__item nav__item--dropdown align-self-center">
				        <a href="#" id="navbarDropdown" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">Career Resources</a>
				        <div class="dropdown-menu" aria-labelledby="navbarDropdown">
				          <a href="#">Action</a>
				          <a href="#">Another action</a>
				        </div>
				      </li>
				      <li class="nav__item align-self-center">
				        <a href="#">Contact Us</a>
				      </li>
				      <li class="nav__item align-self-center">
				        <a href="#" id="signupdropdown" class="btn btn-primary btn-sm" data-toggle="modal" data-target=".signup-modal">Sign Up</a>
				      </li>
				      <li class="nav__item align-self-center">
				        <a href="#" data-toggle="modal" data-target=".login-modal">Log In</a>
				      </li>
				    </ul>
				  </div>
				</nav>
			</div>
		</header>