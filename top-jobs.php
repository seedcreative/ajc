<div class="jobs__top">
	<h2>
		<div class="icon jobs__top--circle">
			<i class="aj-topjobs"></i>
		</div>Top Jobs
	</h2>
	<div class="jobs__top__wrap">
		<div class="row no-gutters">
			<div class="col-md-4">
				<div class="jobs__top__item">
					<div class="jobs__top__abs d-flex">
						<figure class="jobs__top__image">
							<img src="images/company.png" alt="New Hope Agro Business">
						</figure>
						<div class="jobs__top__det">
							<h5>
								<a href="">New Hope Agro Business</a>
							</h5>
							<ul class="jobs__top__list">
								<li>
									<a href="#">Chinese Translator</a>
								</li>
								<li>
									<a href="#">Sales and Marketing Executive</a>
								</li>
								<li>
									<a href="#">Project Lead</a>
								</li>
								<li>
									<a href="#">Receptionist</a>
								</li>
							</ul>
						</div>
					</div>
					
					<div class="jobs__more">
						<i class="aj-down-arrow"></i>
					</div>
				</div>
			</div>
			<div class="col-md-4">
				<div class="jobs__top__item">
					<div class="jobs__top__abs d-flex">
						<figure class="jobs__top__image">
							<img src="images/heal-home-care.png" alt="New Hope Agro Business">
						</figure>
						<div class="jobs__top__det">
							<h5>
								<a href="">Heal Home Care</a>
							</h5>
							<ul class="jobs__top__list">
								<li>
									<a href="#">Nurses (Urgent Requirement)</a>
								</li>
							</ul>
						</div>
					</div>
				</div>
			</div>
			<div class="col-md-4">
				<div class="jobs__top__item">
					<div class="jobs__top__abs d-flex">
						<figure class="jobs__top__image">
							<img src="images/khoj-info.png" alt="New Hope Agro Business">
						</figure>
						<div class="jobs__top__det">
							<h5>
								<a href="">Khoj Informatics</a>
							</h5>
							<ul class="jobs__top__list">
								<li>
									<a href="#">Sales and Marketing</a>
								</li>
							</ul>
						</div>
					</div>
				</div>
			</div>
		</div>
		<div class="row no-gutters">
			<div class="col-md-4">
				<div class="jobs__top__item">
					<div class="jobs__top__abs d-flex">
						<figure class="jobs__top__image">
							<img src="images/company.png" alt="New Hope Agro Business">
						</figure>
						<div class="jobs__top__det">
							<h5>
								<a href="">New Hope Agro Business</a>
							</h5>
							<ul class="jobs__top__list">
								<li>
									<a href="#">Chinese Translator</a>
								</li>
								<li>
									<a href="#">Sales and Marketing Executive</a>
								</li>
								<li>
									<a href="#">Project Lead</a>
								</li>
								<li>
									<a href="#">Receptionist</a>
								</li>
							</ul>
						</div>
					</div>
					
					<div class="jobs__more">
						<i class="aj-down-arrow"></i>
					</div>
				</div>
			</div>
			<div class="col-md-4">
				<div class="jobs__top__item">
					<div class="jobs__top__abs d-flex">
						<figure class="jobs__top__image">
							<img src="images/heal-home-care.png" alt="New Hope Agro Business">
						</figure>
						<div class="jobs__top__det">
							<h5>
								<a href="">Heal Home Care</a>
							</h5>
							<ul class="jobs__top__list">
								<li>
									<a href="#">Nurses (Urgent Requirement)</a>
								</li>
							</ul>
						</div>
					</div>
				</div>
			</div>
			<div class="col-md-4">
				<div class="jobs__top__item">
					<div class="jobs__top__abs d-flex">
						<figure class="jobs__top__image">
							<img src="images/khoj-info.png" alt="New Hope Agro Business">
						</figure>
						<div class="jobs__top__det">
							<h5>
								<a href="">Khoj Informatics</a>
							</h5>
							<ul class="jobs__top__list">
								<li>
									<a href="#">Sales and Marketing</a>
								</li>
							</ul>
						</div>
					</div>
				</div>
			</div>
		</div>
		<div class="row no-gutters">
			
			<div class="col-md-4">
				<div class="jobs__top__item">
					<div class="jobs__top__abs d-flex">
						<figure class="jobs__top__image">
							<img src="images/heal-home-care.png" alt="New Hope Agro Business">
						</figure>
						<div class="jobs__top__det">
							<h5>
								<a href="">Heal Home Care</a>
							</h5>
							<ul class="jobs__top__list">
								<li>
									<a href="#">Nurses (Urgent Requirement)</a>
								</li>
							</ul>
						</div>
					</div>
				</div>
			</div>
			<div class="col-md-4">
				<div class="jobs__top__item">
					<div class="jobs__top__abs d-flex">
						<figure class="jobs__top__image">
							<img src="images/khoj-info.png" alt="New Hope Agro Business">
						</figure>
						<div class="jobs__top__det">
							<h5>
								<a href="">Khoj Informatics</a>
							</h5>
							<ul class="jobs__top__list">
								<li>
									<a href="#">Sales and Marketing</a>
								</li>
							</ul>
						</div>
					</div>
				</div>
			</div>
			<div class="col-md-4">
				<div class="jobs__top__item">
					<div class="jobs__top__abs d-flex">
						<figure class="jobs__top__image">
							<img src="images/company.png" alt="New Hope Agro Business">
						</figure>
						<div class="jobs__top__det">
							<h5>
								<a href="">New Hope Agro Business</a>
							</h5>
							<ul class="jobs__top__list">
								<li>
									<a href="#">Chinese Translator</a>
								</li>
								<li>
									<a href="#">Sales and Marketing Executive</a>
								</li>
								<li>
									<a href="#">Project Lead</a>
								</li>
								<li>
									<a href="#">Receptionist</a>
								</li>
							</ul>
						</div>
					</div>
					
					<div class="jobs__more">
						<i class="aj-down-arrow"></i>
					</div>
				</div>
			</div>
		</div>
		<div class="row no-gutters">
			
			<div class="col-md-4">
				<div class="jobs__top__item">
					<div class="jobs__top__abs d-flex">
						<figure class="jobs__top__image">
							<img src="images/heal-home-care.png" alt="New Hope Agro Business">
						</figure>
						<div class="jobs__top__det">
							<h5>
								<a href="">Heal Home Care</a>
							</h5>
							<ul class="jobs__top__list">
								<li>
									<a href="#">Nurses (Urgent Requirement)</a>
								</li>
							</ul>
						</div>
					</div>
				</div>
			</div>
			<div class="col-md-4">
				<div class="jobs__top__item">
					<div class="jobs__top__abs d-flex">
						<figure class="jobs__top__image">
							<img src="images/company.png" alt="New Hope Agro Business">
						</figure>
						<div class="jobs__top__det">
							<h5>
								<a href="">New Hope Agro Business</a>
							</h5>
							<ul class="jobs__top__list">
								<li>
									<a href="#">Chinese Translator</a>
								</li>
								<li>
									<a href="#">Sales and Marketing Executive</a>
								</li>
								<li>
									<a href="#">Project Lead</a>
								</li>
								<li>
									<a href="#">Receptionist</a>
								</li>
							</ul>
						</div>
					</div>
					
					<div class="jobs__more">
						<i class="aj-down-arrow"></i>
					</div>
				</div>
			</div>
			<div class="col-md-4">
				<div class="jobs__top__item">
					<div class="jobs__top__abs d-flex">
						<figure class="jobs__top__image">
							<img src="images/khoj-info.png" alt="New Hope Agro Business">
						</figure>
						<div class="jobs__top__det">
							<h5>
								<a href="">Khoj Informatics</a>
							</h5>
							<ul class="jobs__top__list">
								<li>
									<a href="#">Sales and Marketing</a>
								</li>
							</ul>
						</div>
					</div>
				</div>
			</div>
		</div>
		<div class="row no-gutters">
			<div class="col-md-4">
				<div class="jobs__top__item">
					<div class="jobs__top__abs d-flex">
						<figure class="jobs__top__image">
							<img src="images/company.png" alt="New Hope Agro Business">
						</figure>
						<div class="jobs__top__det">
							<h5>
								<a href="">New Hope Agro Business</a>
							</h5>
							<ul class="jobs__top__list">
								<li>
									<a href="#">Chinese Translator</a>
								</li>
								<li>
									<a href="#">Sales and Marketing Executive</a>
								</li>
								<li>
									<a href="#">Project Lead</a>
								</li>
								<li>
									<a href="#">Receptionist</a>
								</li>
							</ul>
						</div>
					</div>
					
					<div class="jobs__more">
						<i class="aj-down-arrow"></i>
					</div>
				</div>
			</div>
			<div class="col-md-4">
				<div class="jobs__top__item">
					<div class="jobs__top__abs d-flex">
						<figure class="jobs__top__image">
							<img src="images/khoj-info.png" alt="New Hope Agro Business">
						</figure>
						<div class="jobs__top__det">
							<h5>
								<a href="">Khoj Informatics</a>
							</h5>
							<ul class="jobs__top__list">
								<li>
									<a href="#">Sales and Marketing</a>
								</li>
							</ul>
						</div>
					</div>
				</div>
			</div>
			<div class="col-md-4">
				<div class="jobs__top__item">
					<div class="jobs__top__abs d-flex">
						<figure class="jobs__top__image">
							<img src="images/heal-home-care.png" alt="New Hope Agro Business">
						</figure>
						<div class="jobs__top__det">
							<h5>
								<a href="">Heal Home Care</a>
							</h5>
							<ul class="jobs__top__list">
								<li>
									<a href="#">Nurses (Urgent Requirement)</a>
								</li>
							</ul>
						</div>
					</div>
				</div>
			</div>
		</div>
		<div class="jobs__showmore">
			<a href="#" class="btn btn-primary btn-sm">Load more jobs</a>
		</div>
	</div>
</div>