<?php
	include_once('header.php');
?>
	<main class="main">
		<div class="container">
			<div class="row">
				<aside class="sidebar col-md-3">
					<div class="card sidebar__category">
						<div class="sidebar__category__selected">
							<h2 class="card__title">Your Selected Category</h2>
							<a href="#" class="sidebar__category__selected--item">
								Web Developer
								<span class="aj-cancel"></span>
							</a>
						</div>	
						<div class="sidebar__category__list">
							<h2 class="card__title">Categories</h2>
							<ul class="list">
								<li>
									<a href="#">Administration</a>
								</li>
								<li>
									<a href="#">Financial Services</a>
								</li>
								<li>
									<a href="#">Education</a>
								</li>
								<li>
									<a href="#">Sales &amp; Marketing</a>
								</li>
								<li>
									<a href="#">Web Development</a>
								</li>
								<li>
									<a href="#">Hardware &amp; Networking</a>
								</li>
								<li>
									<a href="#">Banking Institutions</a>
								</li>
								<li>
									<a href="#">Website Design</a>
								</li>
								<li>
									<a href="#">Graphic Design</a>
								</li>
							</ul>
						</div>
					</div>
					<div class="ads">
						<img src="images/ads.gif" alt="Fair and Lovely Ad" class="img-fluid">
					</div>
					<div class="ads">
						<img src="images/ads_grey.jpg" alt="Fair and Lovely Ad" class="img-fluid">
					</div>
				</aside>
				<div class="content col-md-9">
					<table id="jobstable" class="jobstable table table-responsive-md">
						<thead>
							<tr>
								<th>ID</th>
								<th>Image</th>
								<th>Details</th>
								<th>Location</th>
								<th>Type</th>
							</tr>
						</thead>
						<tbody>
							<tr class="jobstable__item">
								<td>1</td>
								<td class="jobstable__image">
									<figure>
										<img src="images/company.png" alt="New Hope Agro Business">
									</figure>
								</td>
								<td class="jobstable__det">
									<h5>
										<a href="#">Lower Secondary English Teacher</a>
									</h5>
									<a href="" class="jobstable__emp">
										Nalanda Vidhya Mandir
									</a>
									<span class="jobstable__deadline">
										<i class="aj-deadline"></i> 234 Days Left
									</span>
								</td>
								<td class="jobstable__emploc">
									<i class="aj-location"></i> Sano Thimi, Bhaktapur
								</td>
								<td>
									<span class="btn btn-sm btn-secondary">Full Time</span>
								</td>
							</tr>
							<tr class="jobstable__item">
								<td>2</td>
								<td class="jobstable__image">
									<figure>
										<img src="images/company.png" alt="New Hope Agro Business">
									</figure>
								</td>
								<td class="jobstable__det">
									<h5>
										<a href="#">Secondary English Teacher</a>
									</h5>
									<a href="" class="jobstable__emp">
										Nalanda Vidhya Mandir
									</a>
									<span class="jobstable__deadline">
										<i class="aj-deadline"></i> 234 Days Left
									</span>
								</td>
								<td class="jobstable__emploc">
									<i class="aj-location"></i> Sano Thimi, Bhaktapur
								</td>
								<td>
									<span class="btn btn-sm btn-secondary">Full Time</span>
								</td>
							</tr>
							<tr class="jobstable__item">
								<td>3</td>
								<td class="jobstable__image">
									<figure>
										<img src="images/company.png" alt="New Hope Agro Business">
									</figure>
								</td>
								<td class="jobstable__det">
									<h5>
										<a href="#">Lower Secondary English Teacher</a>
									</h5>
									<a href="" class="jobstable__emp">
										Nalanda Vidhya Mandir
									</a>
									<span class="jobstable__deadline">
										<i class="aj-deadline"></i> 234 Days Left
									</span>
								</td>
								<td class="jobstable__emploc">
									<i class="aj-location"></i> Sano Thimi, Bhaktapur
								</td>
								<td>
									<span class="btn btn-sm btn-secondary">Full Time</span>
								</td>
							</tr>
							<tr class="jobstable__item">
								<td>4</td>
								<td class="jobstable__image">
									<figure>
										<img src="images/company.png" alt="New Hope Agro Business">
									</figure>
								</td>
								<td class="jobstable__det">
									<h5>
										<a href="#">Lower Secondary English Teacher</a>
									</h5>
									<a href="" class="jobstable__emp">
										Nalanda Vidhya Mandir
									</a>
									<span class="jobstable__deadline">
										<i class="aj-deadline"></i> 234 Days Left
									</span>
								</td>
								<td class="jobstable__emploc">
									<i class="aj-location"></i> Sano Thimi, Bhaktapur
								</td>
								<td>
									<span class="btn btn-sm btn-secondary">Full Time</span>
								</td>
							</tr>
							<tr class="jobstable__item">
								<td>5</td>
								<td class="jobstable__image">
									<figure>
										<img src="images/company.png" alt="New Hope Agro Business">
									</figure>
								</td>
								<td class="jobstable__det">
									<h5>
										<a href="#">Lower Secondary English Teacher</a>
									</h5>
									<a href="" class="jobstable__emp">
										Nalanda Vidhya Mandir
									</a>
									<span class="jobstable__deadline">
										<i class="aj-deadline"></i> 234 Days Left
									</span>
								</td>
								<td class="jobstable__emploc">
									<i class="aj-location"></i> Sano Thimi, Bhaktapur
								</td>
								<td>
									<span class="btn btn-sm btn-secondary">Full Time</span>
								</td>
							</tr>
							<tr class="jobstable__item">
								<td>6</td>
								<td class="jobstable__image">
									<figure>
										<img src="images/company.png" alt="New Hope Agro Business">
									</figure>
								</td>
								<td class="jobstable__det">
									<h5>
										<a href="#">Secondary English Teacher</a>
									</h5>
									<a href="" class="jobstable__emp">
										Nalanda Vidhya Mandir
									</a>
									<span class="jobstable__deadline">
										<i class="aj-deadline"></i> 234 Days Left
									</span>
								</td>
								<td class="jobstable__emploc">
									<i class="aj-location"></i> Sano Thimi, Bhaktapur
								</td>
								<td>
									<span class="btn btn-sm btn-secondary">Full Time</span>
								</td>
							</tr>
							<tr class="jobstable__item">
								<td>7</td>
								<td class="jobstable__image">
									<figure>
										<img src="images/company.png" alt="New Hope Agro Business">
									</figure>
								</td>
								<td class="jobstable__det">
									<h5>
										<a href="#">Lower Secondary English Teacher</a>
									</h5>
									<a href="" class="jobstable__emp">
										Nalanda Vidhya Mandir
									</a>
									<span class="jobstable__deadline">
										<i class="aj-deadline"></i> 234 Days Left
									</span>
								</td>
								<td class="jobstable__emploc">
									<i class="aj-location"></i> Sano Thimi, Bhaktapur
								</td>
								<td>
									<span class="btn btn-sm btn-secondary">Full Time</span>
								</td>
							</tr>
							<tr class="jobstable__item">
								<td>8</td>
								<td class="jobstable__image">
									<figure>
										<img src="images/company.png" alt="New Hope Agro Business">
									</figure>
								</td>
								<td class="jobstable__det">
									<h5>
										<a href="#">Lower Secondary English Teacher</a>
									</h5>
									<a href="" class="jobstable__emp">
										Nalanda Vidhya Mandir
									</a>
									<span class="jobstable__deadline">
										<i class="aj-deadline"></i> 234 Days Left
									</span>
								</td>
								<td class="jobstable__emploc">
									<i class="aj-location"></i> Sano Thimi, Bhaktapur
								</td>
								<td>
									<span class="btn btn-sm btn-secondary">Full Time</span>
								</td>
							</tr>
							<tr class="jobstable__item">
								<td>9</td>
								<td class="jobstable__image">
									<figure>
										<img src="images/company.png" alt="New Hope Agro Business">
									</figure>
								</td>
								<td class="jobstable__det">
									<h5>
										<a href="#">Lower Secondary English Teacher</a>
									</h5>
									<a href="" class="jobstable__emp">
										Nalanda Vidhya Mandir
									</a>
									<span class="jobstable__deadline">
										<i class="aj-deadline"></i> 234 Days Left
									</span>
								</td>
								<td class="jobstable__emploc">
									<i class="aj-location"></i> Sano Thimi, Bhaktapur
								</td>
								<td>
									<span class="btn btn-sm btn-secondary">Full Time</span>
								</td>
							</tr>
							<tr class="jobstable__item">
								<td>10</td>
								<td class="jobstable__image">
									<figure>
										<img src="images/company.png" alt="New Hope Agro Business">
									</figure>
								</td>
								<td class="jobstable__det">
									<h5>
										<a href="#">Secondary English Teacher</a>
									</h5>
									<a href="" class="jobstable__emp">
										Nalanda Vidhya Mandir
									</a>
									<span class="jobstable__deadline">
										<i class="aj-deadline"></i> 234 Days Left
									</span>
								</td>
								<td class="jobstable__emploc">
									<i class="aj-location"></i> Sano Thimi, Bhaktapur
								</td>
								<td>
									<span class="btn btn-sm btn-secondary">Full Time</span>
								</td>
							</tr>
							<tr class="jobstable__item">
								<td>11</td>
								<td class="jobstable__image">
									<figure>
										<img src="images/company.png" alt="New Hope Agro Business">
									</figure>
								</td>
								<td class="jobstable__det">
									<h5>
										<a href="#">Lower Secondary English Teacher</a>
									</h5>
									<a href="" class="jobstable__emp">
										Nalanda Vidhya Mandir
									</a>
									<span class="jobstable__deadline">
										<i class="aj-deadline"></i> 234 Days Left
									</span>
								</td>
								<td class="jobstable__emploc">
									<i class="aj-location"></i> Sano Thimi, Bhaktapur
								</td>
								<td>
									<span class="btn btn-sm btn-secondary">Full Time</span>
								</td>
							</tr>
							<tr class="jobstable__item">
								<td>12</td>
								<td class="jobstable__image">
									<figure>
										<img src="images/company.png" alt="New Hope Agro Business">
									</figure>
								</td>
								<td class="jobstable__det">
									<h5>
										<a href="#">Lower Secondary English Teacher</a>
									</h5>
									<a href="" class="jobstable__emp">
										Nalanda Vidhya Mandir
									</a>
									<span class="jobstable__deadline">
										<i class="aj-deadline"></i> 234 Days Left
									</span>
								</td>
								<td class="jobstable__emploc">
									<i class="aj-location"></i> Sano Thimi, Bhaktapur
								</td>
								<td>
									<span class="btn btn-sm btn-secondary">Full Time</span>
								</td>
							</tr>
						</tbody>
					</table>
				</div>	
			</div>
			
		</div>
		
	</main>
<?php
	include_once('footer.php');
?>