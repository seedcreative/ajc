<div class="search section-pad">
	<div class="search__overlay"></div>
	<div class="video">
	    <div class="video__container">
	        <div class="video__filter"></div>
	        <video autoplay loop class="fillWidth">
	            <source src="video/video.mp4" type="video/mp4" />Your browser does not support the video tag. I suggest you upgrade your browser.
	            <source src="video/video.webm" type="video/webm" />Your browser does not support the video tag. I suggest you upgrade your browser.
	        </video>
	    </div>
	</div>

	<div class="container">
		<h1>View Over 10,000 Jobs Openings Weekly</h1>
		<div class="row">
			<div class="search__form col-md-10 offset-md-1">
				<div class="row">
					<form class="container">
						<div class="row">
							<div class="col-md-6 form-group">
								<input id="job-field" type="text" name="jobs" class="form-control" placeholder="All Jobs...">
							</div>
							<div class="col-md-4 form-group">
								<input id="location-field" type="text" name="jobs" class="form-control" placeholder="Anywhere...">
							</div>
							<button type="button" class="btn btn-secondary align-self-start">Search</button>	
						</div>
					</form>
				</div>
			</div>
		</div>
	</div>
</div>