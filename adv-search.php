<?php
	include_once('header.php');
?>
	<main class="main">
		<div class="container">
			<div class="row">
				<aside class="sidebar col-md-3">
					<div class="card advsearch">
						<h2 class="card__title">Advanced Search</h2>
						<form class="advsearch__form">
							<div class="form-group">
								<label>Company</label>
								<input name="" type="text" class="form-control form-dark form-round form-md" placeholder="Enter Company Name">
							</div>
							<div class="form-group">
								<label>Job Title</label>
								<select class="form-control form-dark form-round form-md">
									<option>- Select Job Title -</option>
									<option>ABC</option>
									<option>DEF</option>
									<option>GHI</option>
									<option>JKL</option>
									<option>MNO</option>
								</select>
							</div>
							<div class="form-group">
								<label>Industry</label>
								<select class="form-control form-dark form-round form-md">
									<option>- Select Industry -</option>
									<option>ABC</option>
									<option>DEF</option>
									<option>GHI</option>
									<option>JKL</option>
									<option>MNO</option>
								</select>
							</div>
							<div class="form-group">
								<label>Job Type</label>
								<div class="form-check">
									<input type="checkbox" class="form-check-input" id="fulltime">
									<label class="form-check-label" for="fulltime">Full Time</label>
								</div>
								<div class="form-check">
									<input type="checkbox" class="form-check-input" id="parttime">
									<label class="form-check-label" for="parttime">Part Time</label>
								</div>
								<div class="form-check">
									<input type="checkbox" class="form-check-input" id="contract">
									<label class="form-check-label" for="contract">Contract</label>
								</div>
							</div>
							<div class="form-group">
								<label>Qualification</label>
								<select class="form-control form-dark form-round">
									<option>- Select Qualification -</option>
									<option>ABC</option>
									<option>DEF</option>
									<option>GHI</option>
									<option>JKL</option>
									<option>MNO</option>
								</select>
							</div>
							<div class="form-group">
								<label>Location</label>
								<input name="" type="text" class="form-control form-dark form-round" placeholder="Enter a Location">
							</div>
							<button class="btn btn-primary btn-md btn-round">Search</button>
						</form>
					</div>
					<div class="ads">
						<img src="images/ads.gif" alt="Fair and Lovely Ad" class="img-fluid">
					</div>
					<div class="ads">
						<img src="images/ads_grey.jpg" alt="Fair and Lovely Ad" class="img-fluid">
					</div>
				</aside>
				<div class="content col-md-9">
					<table id="jobstable" class="jobstable table table-responsive-md">
						<thead>
							<tr>
								<th>ID</th>
								<th>Image</th>
								<th>Details</th>
								<th>Location</th>
								<th>Type</th>
							</tr>
						</thead>
						<tbody>
							<tr class="jobstable__item">
								<td>1</td>
								<td class="jobstable__image">
									<figure>
										<img src="images/company.png" alt="New Hope Agro Business">
									</figure>
								</td>
								<td class="jobstable__det">
									<h5>
										<a href="#">Lower Secondary English Teacher</a>
									</h5>
									<a href="" class="jobstable__emp">
										Nalanda Vidhya Mandir
									</a>
									<span class="jobstable__deadline">
										<i class="aj-deadline"></i> 234 Days Left
									</span>
								</td>
								<td class="jobstable__emploc">
									<i class="aj-location"></i> New Baneshwor, Kathmandu
								</td>
								<td>
									<span class="btn btn-sm btn-secondary">Part Time</span>
								</td>
							</tr>
							<tr class="jobstable__item">
								<td>2</td>
								<td class="jobstable__image">
									<figure>
										<img src="images/company.png" alt="New Hope Agro Business">
									</figure>
								</td>
								<td class="jobstable__det">
									<h5>
										<a href="#">Secondary English Teacher</a>
									</h5>
									<a href="" class="jobstable__emp">
										Nalanda Vidhya Mandir
									</a>
									<span class="jobstable__deadline">
										<i class="aj-deadline"></i> 234 Days Left
									</span>
								</td>
								<td class="jobstable__emploc">
									<i class="aj-location"></i> Sano Thimi, Bhaktapur
								</td>
								<td>
									<span class="btn btn-sm btn-secondary">Full Time</span>
								</td>
							</tr>
							<tr class="jobstable__item">
								<td>3</td>
								<td class="jobstable__image">
									<figure>
										<img src="images/company.png" alt="New Hope Agro Business">
									</figure>
								</td>
								<td class="jobstable__det">
									<h5>
										<a href="#"> Secondary English Teacher</a>
									</h5>
									<a href="" class="jobstable__emp">
										Nalanda Vidhya Mandir
									</a>
									<span class="jobstable__deadline">
										<i class="aj-deadline"></i> 234 Days Left
									</span>
								</td>
								<td class="jobstable__emploc">
									<i class="aj-location"></i> New Baneshwor, Kathmandu
								</td>
								<td>
									<span class="btn btn-sm btn-secondary">Full Time</span>
								</td>
							</tr>
							<tr class="jobstable__item">
								<td>4</td>
								<td class="jobstable__image">
									<figure>
										<img src="images/company.png" alt="New Hope Agro Business">
									</figure>
								</td>
								<td class="jobstable__det">
									<h5>
										<a href="#">Lower Secondary English Teacher</a>
									</h5>
									<a href="" class="jobstable__emp">
										Nalanda Vidhya Mandir
									</a>
									<span class="jobstable__deadline">
										<i class="aj-deadline"></i> 234 Days Left
									</span>
								</td>
								<td class="jobstable__emploc">
									<i class="aj-location"></i> Sano Thimi, Bhaktapur
								</td>
								<td>
									<span class="btn btn-sm btn-secondary">Full Time</span>
								</td>
							</tr>
							<tr class="jobstable__item">
								<td>5</td>
								<td class="jobstable__image">
									<figure>
										<img src="images/company.png" alt="New Hope Agro Business">
									</figure>
								</td>
								<td class="jobstable__det">
									<h5>
										<a href="#">Lower Secondary English Teacher</a>
									</h5>
									<a href="" class="jobstable__emp">
										Nalanda Vidhya Mandir
									</a>
									<span class="jobstable__deadline">
										<i class="aj-deadline"></i> 234 Days Left
									</span>
								</td>
								<td class="jobstable__emploc">
									<i class="aj-location"></i> Sano Thimi, Bhaktapur
								</td>
								<td>
									<span class="btn btn-sm btn-secondary">Temporary</span>
								</td>
							</tr>
							<tr class="jobstable__item">
								<td>6</td>
								<td class="jobstable__image">
									<figure>
										<img src="images/company.png" alt="New Hope Agro Business">
									</figure>
								</td>
								<td class="jobstable__det">
									<h5>
										<a href="#">Secondary English Teacher</a>Temporary</h5>
									<a href="" class="jobstable__emp">
										Nalanda Vidhya Mandir
									</a>
									<span class="jobstable__deadline">
										<i class="aj-deadline"></i> 234 Days Left
									</span>
								</td>
								<td class="jobstable__emploc">
									<i class="aj-location"></i> Old Baneshwor, Kathmandu
								</td>
								<td>
									<span class="btn btn-sm btn-secondary">Contract</span>
								</td>
							</tr>
							<tr class="jobstable__item">
								<td>7</td>
								<td class="jobstable__image">
									<figure>
										<img src="images/company.png" alt="New Hope Agro Business">
									</figure>
Old Baneshwor, Kathmandu
								<td class="jobstable__det">
									<h5>
										<a href="#">Lower Secondary Contract</a>
									</h5>
									<a href="" class="jobstable__emp">
										Nalanda Vidhya Mandir
									</a>
									<span class="jobstable__deadline">
										<i class="aj-deadline"></i> 234 Days Left
									</span>
								</td>
								<td class="jobstable__emploc">
									<i class="aj-location"></i> Sano Thimi, Bhaktapur
								</td>
								<td>
									<span class="btn btn-sm btn-secondary">Full Time</span>
								</td>
							</tr>
							<tr class="jobstable__item">
								<td>8</td>
								<td class="jobstable__image">
									<figure>
										<img src="images/company.png" alt="New Hope Agro Business">
									</figure>
								</td>
								<td class="jobstable__det">
									<h5>
										<a href="#">Lower Secondary English Teacher</a>
									</h5>
									<a href="" class="jobstable__emp">
										Nalanda Vidhya Mandir
									</a>
									<span class="jobstable__deadline">
										<i class="aj-deadline"></i> 234 Days Left
									</span>
								</td>
								<td class="jobstable__emploc">
									<i class="aj-location"></i> Sano Thimi, Bhaktapur
								</td>
								<td>
									<span class="btn btn-sm btn-secondary">Full Time</span>
								</td>
							</tr>
							<tr class="jobstable__item">
								<td>9</td>
								<td class="jobstable__image">
									<figure>
										<img src="images/company.png" alt="New Hope Agro Business">
									</figure>
								</td>
								<td class="jobstable__det">
									<h5>
										<a href="#">Lower Secondary English Teacher</a>
									</h5>
									<a href="" class="jobstable__emp">
										Nalanda Vidhya Mandir
									</a>
									<span class="jobstable__deadline">
										<i class="aj-deadline"></i> 234 Days Left
									</span>
								</td>
								<td class="jobstable__emploc">
									<i class="aj-location"></i> Sano Thimi, Bhaktapur
								</td>
								<td>
									<span class="btn btn-sm btn-secondary">Full Time</span>
								</td>
							</tr>
							<tr class="jobstable__item">
								<td>10</td>
								<td class="jobstable__image">
									<figure>
										<img src="images/company.png" alt="New Hope Agro Business">
									</figure>
								</td>
								<td class="jobstable__det">
									<h5>
										<a href="#">Secondary English Teacher</a>
									</h5>
									<a href="" class="jobstable__emp">
										Nalanda Vidhya Mandir
									</a>
									<span class="jobstable__deadline">
										<i class="aj-deadline"></i> 234 Days Left
									</span>
								</td>
								<td class="jobstable__emploc">
									<i class="aj-location"></i> Sano Thimi, Bhaktapur
								</td>
								<td>
									<span class="btn btn-sm btn-secondary">Full Time</span>
								</td>
							</tr>
							<tr class="jobstable__item">
								<td>11</td>
								<td class="jobstable__image">
									<figure>
										<img src="images/company.png" alt="New Hope Agro Business">
									</figure>
								</td>
								<td class="jobstable__det">
									<h5>
										<a href="#">Lower Secondary English Teacher</a>
									</h5>
									<a href="" class="jobstable__emp">
										Nalanda Vidhya Mandir
									</a>
									<span class="jobstable__deadline">
										<i class="aj-deadline"></i> 234 Days Left
									</span>
								</td>
								<td class="jobstable__emploc">
									<i class="aj-location"></i> Sano Thimi, Bhaktapur
								</td>
								<td>
									<span class="btn btn-sm btn-secondary">Full Time</span>
								</td>
							</tr>
							<tr class="jobstable__item">
								<td>12</td>
								<td class="jobstable__image">
									<figure>
										<img src="images/company.png" alt="New Hope Agro Business">
									</figure>
								</td>
								<td class="jobstable__det">
									<h5>
										<a href="#">Lower Secondary English Teacher</a>
									</h5>
									<a href="" class="jobstable__emp">
										Nalanda Vidhya Mandir
									</a>
									<span class="jobstable__deadline">
										<i class="aj-deadline"></i> 234 Days Left
									</span>
								</td>
								<td class="jobstable__emploc">
									<i class="aj-location"></i> Sano Thimi, Bhaktapur
								</td>
								<td>
									<span class="btn btn-sm btn-secondary">Full Time</span>
								</td>
							</tr>
						</tbody>
					</table>
				</div>	
			</div>
			
		</div>
		
	</main>
<?php
	include_once('footer.php');
?>