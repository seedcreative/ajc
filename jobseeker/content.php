<div class="col-md-6">
	<div class="card">
		<h2 class="card__title">Jobs matching your skills <a href="#" class="card__title--link">view all</a></h2>
		<div class="jobs">
			<div class="jobs__na">
				<div class="alert alert-info" role="alert">
				  <i class="fa fa-info-circle"></i>
				  <p>Currently, there are no jobs matching your skills.</p>
				</div>
			</div>
			<div class="jobs__listwrap">
				<div class="jobs__listitem d-flex align-items-center">
					<div class="jobs__itemimage">
						<figure>
							<img src="images/employer.jpg" alt="Circle Lines Media" class="img-fluid">
						</figure>
					</div>
					<div class="jobs__itemdet">
						<span class="jobs__itemcompany">
							<a href="#" class="jobs__itemcompany--link">Circle Lines Media</a> is looking for
						</span>
						<h5 class="jobs__itemtitle">
							<a href="#">Front End Developer</a>
						</h5>
						<div class="jobs__deadline">
							<i class="aj-deadline"></i> 234 days left
						</div>
					</div>
					<div class="jobs__itemtype ml-auto">
						<span class="btn btn-secondary btn-sm">Full Time</span>
					</div>
				</div>
				<div class="jobs__listitem d-flex align-items-center">
					<div class="jobs__itemimage">
						<figure>
							<img src="images/employer.jpg" alt="Circle Lines Media" class="img-fluid">
						</figure>
					</div>
					<div class="jobs__itemdet">
						<span class="jobs__itemcompany">
							<a href="#" class="jobs__itemcompany--link">Circle Lines Media</a> is looking for
						</span>
						<h5 class="jobs__itemtitle">
							<a href="#">Front End Developer</a>
						</h5>
						<div class="jobs__deadline">
							<i class="aj-deadline"></i> 234 days left
						</div>
					</div>
					<div class="jobs__itemtype ml-auto">
						<span class="btn btn-secondary btn-sm">Full Time</span>
					</div>
				</div>
				<div class="jobs__listitem d-flex align-items-center">
					<div class="jobs__itemimage">
						<figure>
							<img src="images/employer.jpg" alt="Circle Lines Media" class="img-fluid">
						</figure>
					</div>
					<div class="jobs__itemdet">
						<span class="jobs__itemcompany">
							<a href="#" class="jobs__itemcompany--link">Circle Lines Media</a> is looking for
						</span>
						<h5 class="jobs__itemtitle">
							<a href="#">Front End Developer</a>
						</h5>
						<div class="jobs__deadline">
							<i class="aj-deadline"></i> 234 days left
						</div>
					</div>
					<div class="jobs__itemtype ml-auto">
						<span class="btn btn-secondary btn-sm">Full Time</span>
					</div>
				</div>
				<div class="jobs__listitem d-flex align-items-center">
					<div class="jobs__itemimage">
						<figure>
							<img src="images/employer.jpg" alt="Circle Lines Media" class="img-fluid">
						</figure>
					</div>
					<div class="jobs__itemdet">
						<span class="jobs__itemcompany">
							<a href="#" class="jobs__itemcompany--link">Circle Lines Media</a> is looking for
						</span>
						<h5 class="jobs__itemtitle">
							<a href="#">Front End Developer</a>
						</h5>
						<div class="jobs__deadline">
							<i class="aj-deadline"></i> 234 days left
						</div>
					</div>
					<div class="jobs__itemtype ml-auto">
						<span class="btn btn-secondary btn-sm">Full Time</span>
					</div>
				</div>
				<div class="jobs__listitem d-flex align-items-center">
					<div class="jobs__itemimage">
						<figure>
							<img src="images/employer.jpg" alt="Circle Lines Media" class="img-fluid">
						</figure>
					</div>
					<div class="jobs__itemdet">
						<span class="jobs__itemcompany">
							<a href="#" class="jobs__itemcompany--link">Circle Lines Media</a> is looking for
						</span>
						<h5 class="jobs__itemtitle">
							<a href="#">Front End Developer</a>
						</h5>
						<div class="jobs__deadline">
							<i class="aj-deadline"></i> 234 days left
						</div>
					</div>
					<div class="jobs__itemtype ml-auto">
						<span class="btn btn-secondary btn-sm">Full Time</span>
					</div>
				</div>
			</div>
		</div>		
	</div>
</div>
<div class="col-md-3">
	<div class="card accinfo">
		<h2 class="card__title">Accounts Information</h2>
		<div class="accinfo__jobsapplied">
			<span>21</span>
			<p>Jobs Applied</p>
		</div>
		<div class="sidenav">
			<ul class="sidenav__menu">
				<li class="sidenav__item">
					<a href="#">
						<i class="fa fa-file-alt"></i>
						See my profile as others
					</a>
				</li>
				<li class="sidenav__item">
					<a href="#">
						<i class="fa fa-file-pdf"></i>
						Convert my resume to PDF
					</a>
				</li>
			</ul>
		</div>
	</div>
	<div class="ads">
		<img src="images/ads.gif" alt="Fair and Lovely Ad" class="img-fluid">
	</div>
	<div class="card jobbasket">
		<h2 class="card__title">Job Basket <a href="#" class="card__title--link">view all</a></h2>
		<ul class="jobbasket__list">
			<li class="jobbasket__item d-flex">
				<div class="jobbasket__image">
					<figure>
						<img src="images/employer.jpg" alt="Circle Lines Media" class="img-fluid">
					</figure>
				</div>
				<div class="jobbasket__det">
					<h5>
						<a href="#">Front End Developer</a>
					</h5>
					<span>at <a href="#">Circle Lines Media</a></span>
				</div>
			</li>
			<li class="jobbasket__item d-flex">
				<div class="jobbasket__image">
					<figure>
						<img src="images/employer.jpg" alt="Circle Lines Media" class="img-fluid">
					</figure>
				</div>
				<div class="jobbasket__det">
					<h5>
						<a href="#">Front End Developer</a>
					</h5>
					<span>at <a href="#">Circle Lines Media</a></span>
				</div>
			</li>
			<li class="jobbasket__item d-flex">
				<div class="jobbasket__image">
					<figure>
						<img src="images/employer.jpg" alt="Circle Lines Media" class="img-fluid">
					</figure>
				</div>
				<div class="jobbasket__det">
					<h5>
						<a href="#">Front End Developer</a>
					</h5>
					<span>at <a href="#">Circle Lines Media</a></span>
				</div>
			</li>
		</ul>
	</div>
</div>