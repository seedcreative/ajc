<aside class="col-md-3">
	<div class="card">
		<div class="usercard d-flex">
			<div class="usercard__image">
				<figure>
					<img src="images/usercard-image.jpg" alt="Michelle Obama">
				</figure>
			</div>
			<div class="usercard__det">
				<h5 class="usercard__name">Michelle Obama</h5>
				<a href="" class="usercard__edit">Edit Profile</a>
			</div>
		</div>
		<!--Profile Completeness-->
		<div class="profileload">
			<h5 class="profileload__title">Profile Completeness: <span>25%</span></h5>
			<div class="profileload__progress">
				<div class="progress">
				  <div class="progress-bar bg-danger" role="progressbar" aria-valuenow="25" aria-valuemin="0" aria-valuemax="100" style="width: 25%;">
				    <span class="sr-only">25% Complete</span>
				  </div>
				</div>
			</div>
			<span class="profileload__text">Complete your profile to 100% to increase the chance of getting shortlisted for the right job!</span>
		</div>
		<!--Dashboard Link-->
		<div class="sidenav">
			<ul class="sidenav__menu">
				<li class="sidenav__item sidenav__item--active">
					<a href="#">
						<i class="fa fa-tachometer-alt"></i>
						Dashboard
					</a>
				</li>
				<li class="sidenav__item">
					<a href="#">
						<i class="fa fa-file-alt"></i>
						My Resume
					</a>
				</li>
				<li class="sidenav__item">
					<a href="#">
						<i class="fa fa-comments"></i>
						Messages
					</a>
					<span class="sidenav__itemcount">3</span>
				</li>
				<li class="sidenav__item">
					<a href="#">
						<i class="fa fa-shopping-basket"></i>
						Job Basket
					</a>
				</li>
				<li class="sidenav__item">
					<a href="#">
						<i class="fa fa-archive"></i>
						Jobs I've applied to
					</a>
				</li>
				<li class="sidenav__item">
					<a href="#">
						<i class="fa fa-key"></i>
						Change Password
					</a>
				</li>
			</ul>
		</div>
	</div>
	<div class="ads">
		<img src="images/ads.gif" alt="Fair and Lovely Ad" class="img-fluid">
	</div>
</aside>