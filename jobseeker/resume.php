<div class="col-md-9">
	<div class="card resume">
		<div class="row">
			<div class="col-md-12 resume__block resume__cover">
				<div class="row">
					<div class="col-md-3">
						<figure class="resume__avatar rounded-circle">
							<img src="images/avatar.jpg" alt="Michelle Obama" class="img-fluid">	
						</figure>
					</div>
					<div class="col-md-9">
						<h1 class="resume__name">Michelle Obama</h1 >
						<ul class="resume__detail">
							<li class="d-flex align-items-center">
								<i class="aj-location"></i>
								<span>Khumaltar, Lalitpur Sub-Metropolitan City, Central Development Region, Nepal</span>
							</li>
							<li class="d-flex align-items-center">
								<i class="aj-phone"></i>
								<span>9803676858, 021536569</span>
							</li>
							<li class="d-flex align-items-center">
								<i class="aj-email"></i>
								<span>kamleshthewebdesigner@gmail.com</span>
							</li>
						</ul>
					</div>
				</div>
			</div>
			<div class="col-md-12 resume__block resume__experience">
				<h2 class="resume__heading">Work Experience</h2>
				<div class="row">
					<div class="col-md-4">
						Jun, 2010 - Aug, 2011
					</div>
					<div class="col-md-8">
						<h3>Design Team Lead</h3>
						<span>Global Digital</span>
						<p>The key responsibility produced by the position was to commence a project with the other team member in a smooth way. Take the tasks supervised by the team leader and perform those distributing the work to other team members according to their skills and experience.</p>
					</div>
				</div>
				<div class="row">
					<div class="col-md-4">
						Jun, 2010 - Aug, 2011
					</div>
					<div class="col-md-8">
						<h3>Design Team Lead</h3>
						<span>Global Digital</span>
						<p>The key responsibility produced by the position was to commence a project with the other team member in a smooth way. Take the tasks supervised by the team leader and perform those distributing the work to other team members according to their skills and experience.</p>
					</div>
				</div>
			</div>
			<div class="col-md-12 resume__block resume__education">
				<h2 class="resume__heading">Education</h2>
				<div class="row">
					<div class="col-md-4">2007</div>
					<div class="col-md-8">
						<h3>SLC (10th) School Leaving Certificate 75.00%</h3>
						<span>Don Bosco School</span>
					</div>
				</div>
				<div class="row">
					<div class="col-md-4">2007</div>
					<div class="col-md-8">
						<h3>SLC (10th) School Leaving Certificate 75.00%</h3>
						<span>Don Bosco School</span>
					</div>
				</div>
			</div>
			<div class="col-md-12 resume__block resume__training">
				<h2 class="resume__heading">Trainings</h2>
				<div class="row">
					<div class="col-md-4">2007</div>
					<div class="col-md-8">
						<h3>PHP Training</h3>
						<span>Seed Institute of Technology</span>
					</div>
				</div>
			</div>
			<div class="col-md-12 resume__block resume__skills">
				<h2 class="resume__heading">Skills</h2>
				<div class="row">
					<div class="col-md-4"></div>
					<div class="col-md-8">
						<h3>Specializations</h3>
						<ul class="resume__block--skills">
							<li>Team Leading</li>
							<li>Communication</li>
						</ul>
					</div>
				</div>
				<div class="row">
					<div class="col-md-4"></div>
					<div class="col-md-8">
						<h3>Skills</h3>
						<ul class="resume__block--skills">
							<li>HTML</li>
							<li>CSS</li>
							<li>JavaScript</li>
							<li>Python</li>
							<li>dJango</li>
						</ul>
					</div>
				</div>
			</div>
			<div class="col-md-12 resume__block resume__jobpreference">
				<h2 class="resume__heading">Job Preferences</h2>
				<div class="row">
					<div class="col-md-4"></div>
					<div class="col-md-8">
						<ul class="resume__block--list">
							<li>Looking for: Top Level</li>
							<li>Available for: Top Level</li>
							<li>Expected Salary: Top Level</li>
							<li>Total Experience: Top Level</li>
							<li>Current Salary: Top Level</li>
						</ul>
					</div>
				</div>
			</div>
			<div class="col-md-12 resume__block resume__reference">
				<h2 class="resume__heading">Reference</h2>
				<div class="row">
					<div class="col-md-4"></div>
					<div class="col-md-8">
						<h3>Subekhya Kathayat</h3>
						<span>Don Bosco School</span>
						<span>9861239656</span>
					</div>
				</div>
			</div>
			<div class="col-md-12 resume__block resume__personalinfo">
				<h2 class="resume__heading">Personal Information</h2>
				<div class="row">
					<div class="col-md-4"></div>
					<div class="col-md-8">
						<ul class="resume__block--list">
							<li>Permanent Address: Trimurti Chowk</li>
							<li>Marital Status: Unmarried</li>
							<li>Religion: Hindu</li>
							<li>Nationality: Nepali</li>
						</ul>
					</div>
				</div>
			</div>
		</div>	
	</div>
</div>