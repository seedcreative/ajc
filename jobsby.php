<section class="jobsby">
	<div class="container">
		<h1 class="text-center">Search Jobs by</h1>
		<div class="jobsby__tabs">
			<div class="d-flex jobsby__tabs--heading justify-content-center">
				<ul class="nav" id="jobsby-tabs" role="tablist">
					<li class="nav-item">
						<a class="nav-link active" id="category-tab" data-toggle="tab" href="#category" role="tab" aria-controls="category" aria-selected="true">Categories</a>
					</li>
					<li class="nav-item">
						<a class="nav-link" id="industry-tab" data-toggle="tab" href="#industry" role="tab" aria-controls="industry" aria-selected="false">Industry</a>
					</li>
					<li class="nav-item">
						<a class="nav-link" id="location-tab" data-toggle="tab" href="#location" role="tab" aria-controls="location" aria-selected="false">Location</a>
					</li>
				</ul>
			</div>
			

			<div class="tab-content jobsby__tab--content" id="jobsby-content">
			  <div class="tab-pane fade show active" id="category" role="tabpanel" aria-labelledby="category-tab">
			  	<ul class="row">
			  		<li class="col-lg-3 col-md-6 col-sm-6 jobsby__list">
			  			<a href="#">Accounting and Finance <span>(6)</span></a>
			  		</li>
			  		<li class="col-lg-3 col-md-6 col-sm-6 jobsby__list">
			  			<a href="#">Administrative/Management <span>(20)</span></a>
			  		</li>
			  		<li class="col-lg-3 col-md-6 col-sm-6 jobsby__list">
			  			<a href="#">Business Development <span>(2)</span></a>
			  		</li>
			  		<li class="col-lg-3 col-md-6 col-sm-6 jobsby__list">
			  			<a href="#">Computer - Management <span>(2)</span></a>
			  		</li>
			  		<li class="col-lg-3 col-md-6 col-sm-6 jobsby__list">
			  			<a href="#">Computer - Content Writing <span>(23)</span></a>
			  		</li>
			  		<li class="col-lg-3 col-md-6 col-sm-6 jobsby__list">
			  			<a href="#">Database Management <span>(34)</span></a>
			  		</li>
			  		<li class="col-lg-3 col-md-6 col-sm-6 jobsby__list">
			  			<a href="#">Computer - Hardware/Network <span>(3)</span></a>
			  		</li>
			  		<li class="col-lg-3 col-md-6 col-sm-6 jobsby__list">
			  			<a href="#">Computer - Programming <span>(12)</span></a>
			  		</li>
			  		<li class="col-lg-3 col-md-6 col-sm-6 jobsby__list">
			  			<a href="#">Accounting and Finance <span>(6)</span></a>
			  		</li>
			  		<li class="col-lg-3 col-md-6 col-sm-6 jobsby__list">
			  			<a href="#">Administrative/Management <span>(20)</span></a>
			  		</li>
			  		<li class="col-lg-3 col-md-6 col-sm-6 jobsby__list">
			  			<a href="#">Business Development <span>(2)</span></a>
			  		</li>
			  		<li class="col-lg-3 col-md-6 col-sm-6 jobsby__list">
			  			<a href="#">Computer - Management <span>(2)</span></a>
			  		</li>
			  		<li class="col-lg-3 col-md-6 col-sm-6 jobsby__list">
			  			<a href="#">Computer - Content Writing <span>(23)</span></a>
			  		</li>
			  		<li class="col-lg-3 col-md-6 col-sm-6 jobsby__list">
			  			<a href="#">Database Management <span>(34)</span></a>
			  		</li>
			  		<li class="col-lg-3 col-md-6 col-sm-6 jobsby__list">
			  			<a href="#">Computer - Hardware/Network <span>(3)</span></a>
			  		</li>
			  		<li class="col-lg-3 col-md-6 col-sm-6 jobsby__list">
			  			<a href="#">Computer - Programming <span>(12)</span></a>
			  		</li>
			  		<li class="col-lg-3 col-md-6 col-sm-6 jobsby__list">
			  			<a href="#">Accounting and Finance <span>(6)</span></a>
			  		</li>
			  		<li class="col-lg-3 col-md-6 col-sm-6 jobsby__list">
			  			<a href="#">Administrative/Management <span>(20)</span></a>
			  		</li>
			  		<li class="col-lg-3 col-md-6 col-sm-6 jobsby__list">
			  			<a href="#">Business Development <span>(2)</span></a>
			  		</li>
			  		<li class="col-lg-3 col-md-6 col-sm-6 jobsby__list">
			  			<a href="#">Computer - Management <span>(2)</span></a>
			  		</li>
			  		<li class="col-lg-3 col-md-6 col-sm-6 jobsby__list">
			  			<a href="#">Computer - Content Writing <span>(23)</span></a>
			  		</li>
			  		<li class="col-lg-3 col-md-6 col-sm-6 jobsby__list">
			  			<a href="#">Database Management <span>(34)</span></a>
			  		</li>
			  		<li class="col-lg-3 col-md-6 col-sm-6 jobsby__list">
			  			<a href="#">Computer - Hardware/Network <span>(3)</span></a>
			  		</li>
			  		<li class="col-lg-3 col-md-6 col-sm-6 jobsby__list">
			  			<a href="#">Computer - Programming <span>(12)</span></a>
			  		</li>
			  	</ul>
			  </div>
			  <div class="tab-pane fade" id="industry" role="tabpanel" aria-labelledby="industry-tab">
			  	<ul class="row">
			  		<li class="col-lg-3 col-md-6 col-sm-6 jobsby__list">
			  			<a href="#">Accounting and Finance <span>(6)</span></a>
			  		</li>
			  		<li class="col-lg-3 col-md-6 col-sm-6 jobsby__list">
			  			<a href="#">Administrative/Management <span>(20)</span></a>
			  		</li>
			  		<li class="col-lg-3 col-md-6 col-sm-6 jobsby__list">
			  			<a href="#">Business Development <span>(2)</span></a>
			  		</li>
			  		<li class="col-lg-3 col-md-6 col-sm-6 jobsby__list">
			  			<a href="#">Computer - Management <span>(2)</span></a>
			  		</li>
			  		<li class="col-lg-3 col-md-6 col-sm-6 jobsby__list">
			  			<a href="#">Computer - Content Writing <span>(23)</span></a>
			  		</li>
			  		<li class="col-lg-3 col-md-6 col-sm-6 jobsby__list">
			  			<a href="#">Database Management <span>(34)</span></a>
			  		</li>
			  		<li class="col-lg-3 col-md-6 col-sm-6 jobsby__list">
			  			<a href="#">Computer - Hardware/Network <span>(3)</span></a>
			  		</li>
			  		<li class="col-lg-3 col-md-6 col-sm-6 jobsby__list">
			  			<a href="#">Computer - Programming <span>(12)</span></a>
			  		</li>
			  		<li class="col-lg-3 col-md-6 col-sm-6 jobsby__list">
			  			<a href="#">Accounting and Finance <span>(6)</span></a>
			  		</li>
			  		<li class="col-lg-3 col-md-6 col-sm-6 jobsby__list">
			  			<a href="#">Administrative/Management <span>(20)</span></a>
			  		</li>
			  		<li class="col-lg-3 col-md-6 col-sm-6 jobsby__list">
			  			<a href="#">Business Development <span>(2)</span></a>
			  		</li>
			  		<li class="col-lg-3 col-md-6 col-sm-6 jobsby__list">
			  			<a href="#">Computer - Management <span>(2)</span></a>
			  		</li>
			  		<li class="col-lg-3 col-md-6 col-sm-6 jobsby__list">
			  			<a href="#">Computer - Content Writing <span>(23)</span></a>
			  		</li>
			  		<li class="col-lg-3 col-md-6 col-sm-6 jobsby__list">
			  			<a href="#">Database Management <span>(34)</span></a>
			  		</li>
			  		<li class="col-lg-3 col-md-6 col-sm-6 jobsby__list">
			  			<a href="#">Computer - Hardware/Network <span>(3)</span></a>
			  		</li>
			  		<li class="col-lg-3 col-md-6 col-sm-6 jobsby__list">
			  			<a href="#">Computer - Programming <span>(12)</span></a>
			  		</li>
			  		<li class="col-lg-3 col-md-6 col-sm-6 jobsby__list">
			  			<a href="#">Accounting and Finance <span>(6)</span></a>
			  		</li>
			  		<li class="col-lg-3 col-md-6 col-sm-6 jobsby__list">
			  			<a href="#">Administrative/Management <span>(20)</span></a>
			  		</li>
			  		<li class="col-lg-3 col-md-6 col-sm-6 jobsby__list">
			  			<a href="#">Business Development <span>(2)</span></a>
			  		</li>
			  		<li class="col-lg-3 col-md-6 col-sm-6 jobsby__list">
			  			<a href="#">Computer - Management <span>(2)</span></a>
			  		</li>
			  		<li class="col-lg-3 col-md-6 col-sm-6 jobsby__list">
			  			<a href="#">Computer - Content Writing <span>(23)</span></a>
			  		</li>
			  		<li class="col-lg-3 col-md-6 col-sm-6 jobsby__list">
			  			<a href="#">Database Management <span>(34)</span></a>
			  		</li>
			  		<li class="col-lg-3 col-md-6 col-sm-6 jobsby__list">
			  			<a href="#">Computer - Hardware/Network <span>(3)</span></a>
			  		</li>
			  		<li class="col-lg-3 col-md-6 col-sm-6 jobsby__list">
			  			<a href="#">Computer - Programming <span>(12)</span></a>
			  		</li>
			  	</ul>
			  </div>
			  <div class="tab-pane fade" id="location" role="tabpanel" aria-labelledby="location-tab">
				<ul class="row">
			  		<li class="col-lg-3 col-md-6 col-sm-6 jobsby__list">
			  			<a href="#">Accounting and Finance <span>(6)</span></a>
			  		</li>
			  		<li class="col-lg-3 col-md-6 col-sm-6 jobsby__list">
			  			<a href="#">Administrative/Management <span>(20)</span></a>
			  		</li>
			  		<li class="col-lg-3 col-md-6 col-sm-6 jobsby__list">
			  			<a href="#">Business Development <span>(2)</span></a>
			  		</li>
			  		<li class="col-lg-3 col-md-6 col-sm-6 jobsby__list">
			  			<a href="#">Computer - Management <span>(2)</span></a>
			  		</li>
			  		<li class="col-lg-3 col-md-6 col-sm-6 jobsby__list">
			  			<a href="#">Computer - Content Writing <span>(23)</span></a>
			  		</li>
			  		<li class="col-lg-3 col-md-6 col-sm-6 jobsby__list">
			  			<a href="#">Database Management <span>(34)</span></a>
			  		</li>
			  		<li class="col-lg-3 col-md-6 col-sm-6 jobsby__list">
			  			<a href="#">Computer - Hardware/Network <span>(3)</span></a>
			  		</li>
			  		<li class="col-lg-3 col-md-6 col-sm-6 jobsby__list">
			  			<a href="#">Computer - Programming <span>(12)</span></a>
			  		</li>
			  		<li class="col-lg-3 col-md-6 col-sm-6 jobsby__list">
			  			<a href="#">Accounting and Finance <span>(6)</span></a>
			  		</li>
			  		<li class="col-lg-3 col-md-6 col-sm-6 jobsby__list">
			  			<a href="#">Administrative/Management <span>(20)</span></a>
			  		</li>
			  		<li class="col-lg-3 col-md-6 col-sm-6 jobsby__list">
			  			<a href="#">Business Development <span>(2)</span></a>
			  		</li>
			  		<li class="col-lg-3 col-md-6 col-sm-6 jobsby__list">
			  			<a href="#">Computer - Management <span>(2)</span></a>
			  		</li>
			  		<li class="col-lg-3 col-md-6 col-sm-6 jobsby__list">
			  			<a href="#">Computer - Content Writing <span>(23)</span></a>
			  		</li>
			  		<li class="col-lg-3 col-md-6 col-sm-6 jobsby__list">
			  			<a href="#">Database Management <span>(34)</span></a>
			  		</li>
			  		<li class="col-lg-3 col-md-6 col-sm-6 jobsby__list">
			  			<a href="#">Computer - Hardware/Network <span>(3)</span></a>
			  		</li>
			  		<li class="col-lg-3 col-md-6 col-sm-6 jobsby__list">
			  			<a href="#">Computer - Programming <span>(12)</span></a>
			  		</li>
			  		<li class="col-lg-3 col-md-6 col-sm-6 jobsby__list">
			  			<a href="#">Accounting and Finance <span>(6)</span></a>
			  		</li>
			  		<li class="col-lg-3 col-md-6 col-sm-6 jobsby__list">
			  			<a href="#">Administrative/Management <span>(20)</span></a>
			  		</li>
			  		<li class="col-lg-3 col-md-6 col-sm-6 jobsby__list">
			  			<a href="#">Business Development <span>(2)</span></a>
			  		</li>
			  		<li class="col-lg-3 col-md-6 col-sm-6 jobsby__list">
			  			<a href="#">Computer - Management <span>(2)</span></a>
			  		</li>
			  		<li class="col-lg-3 col-md-6 col-sm-6 jobsby__list">
			  			<a href="#">Computer - Content Writing <span>(23)</span></a>
			  		</li>
			  		<li class="col-lg-3 col-md-6 col-sm-6 jobsby__list">
			  			<a href="#">Database Management <span>(34)</span></a>
			  		</li>
			  		<li class="col-lg-3 col-md-6 col-sm-6 jobsby__list">
			  			<a href="#">Computer - Hardware/Network <span>(3)</span></a>
			  		</li>
			  		<li class="col-lg-3 col-md-6 col-sm-6 jobsby__list">
			  			<a href="#">Computer - Programming <span>(12)</span></a>
			  		</li>
			  	</ul>
			  </div>
			</div>
		</div>
	</div>
</section>