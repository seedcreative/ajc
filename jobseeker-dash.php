<?php
	include_once('header.php');
?>
	<main class="main">
		<div class="container">
			<div class="row">
				<?php include_once('jobseeker/sidebar.php'); ?>
				<?php include_once('jobseeker/content.php'); ?>
			</div>
		</div>
	</main>
<?php
	include_once('footer.php');
?>