<section class="ima row no-gutters">
	<div class="ima__employer col-md-6 col-sm-12">
		<div class="ima__overlay"></div>
		<header>
			<span>I'm an</span>
			Employer
		</header>
		<p>Sign in as a company and be able to post a job and search for candidates...</p>
		<a href="#" class="btn btn-default btn-fluid">Register as a Company</a>
	</div>
	<div class="ima__jobseeker col-md-6 col-sm-12">
		<div class="ima__overlay"></div>
		<header>
			<span>I'm a</span>
			Job Seeker
		</header>
		<p>Sign in as a candidate and be able to find jobs and grow your career...</p>
		<a href="#" class="btn btn-default btn-fluid">Register as a Candidate</a>
	</div>
</section>