<main class="main">
	<div class="container">
		<div class="row">
			<div class="jobs col-md-9">
				<?php 
				include_once('top-jobs.php');
				include_once('latest-jobs.php');

				?>	
			</div>
			<aside class="sidebar col-md-3">
				<?php 
					include_once('ad-sidebar.php');
				?>	
			</aside>
		</div>
	</div>
</main>